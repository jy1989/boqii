const puppeteer = require('puppeteer');
const fs = require('fs');
const download = require('image-downloader');
var rimraf = require('rimraf');


const sources = {
    dog: {
        name: 'dog',
        url: 'http://www.boqii.com/pet-all/dog/'
    },
    cat: {
        name: 'cat',
        url: 'http://www.boqii.com/pet-all/cat/'
    },
    smallpet: {
        name: 'smallpet',
        url: 'http://www.boqii.com/pet-all/smallpet/'
    },
    aquarium: {
        name: 'aquarium',
        url: 'http://www.boqii.com/pet-all/aquarium/'
    },
    reptile: {
        name: 'reptile',
        url: 'http://www.boqii.com/pet-all/reptile/'
    },

};

const source = sources[process.argv[2]];
(async () => {
    function printProgress(progress) {
        process.stdout.clearLine();
        process.stdout.cursorTo(0);
        process.stdout.write(progress + '%');
    }

    const browser = await puppeteer.launch({
        headless: false,
        //devtools: true,
        //slowMo: 50 // slow down by 250ms
        //timeout: 90000
    });

    const page = await browser.newPage();
    const navigationPromise = page.waitForNavigation()
    await page.setViewport({
        width: 1920,
        height: 1080
    })

    await page.setRequestInterception(true);
    page.on('request', interceptedRequest => {
        if (interceptedRequest.url().indexOf('boqii') < 0) { interceptedRequest.abort() }
        else {
            interceptedRequest.continue()
        }
    });


    if (fs.existsSync(`./${source.name}/`)) {
        //fs.rmdirSync(`./${source.name}/`);
        rimraf.sync(`./${source.name}/`);

    }
    fs.mkdirSync(`./${source.name}/`)
    let petsUrl = await findDetailLink(source.url)
    console.log('petsUrl size ', petsUrl.size)
    petsUrl = [...petsUrl]
    console.log('pets ', petsUrl.length)

    let petsInfo = []
    for (let i in petsUrl) {
        let petUrl = petsUrl[i]
        let petInfo = await findDetail(petUrl, parseInt(i) + 1)
        printProgress('progress:'+ (parseInt(i)*100)/petsUrl.length)
        petsInfo.push(petInfo)

    }
    fs.writeFileSync(`./${source.name}/data.json`, JSON.stringify(petsInfo))
    console.log('done')
    await browser.close()


    async function findDetail(url, id) {
        //await page.goto('http://www.boqii.com/entry/detail/437.html')//dog
        await page.goto(url,{
            timeout:0,
            waitUntil:'networkidle2'
        })//cat
        await navigationPromise
        let petInfo = await page.evaluate(() => {
            function strip(html) {
                var tmp = document.createElement("DIV");
                tmp.innerHTML = html;
                let ouput = tmp.textContent || tmp.innerText || "";
                ouput = ouput.trim()
                return ouput
            }

            let name = $('#content > div > div.entry.mt20 > h1').html()
			console.log(name)
			name=name.trim()

            let imgs = new Set()
            $(`img[alt=${name}]`).each(function () {
                imgs.add($(this).attr('src'))
            })

            let entry_tit_pet = []
            $(`.entry_tit_pet dd`).each(function () {
                let key = $(this).find('span').html()
                key = key.replace(' ', '')
                key = key.replace('　', '')
                key = key.replace('：', '')
                let value = $(this).find('em').html()
                entry_tit_pet.push({ key, value })
            })
            let entry_detail = []
            $('.entry_detail').each(function () {
                let value = strip($(this).html())
				value=value.replace(name+'(详情介绍)','')
				value=value.replace('(详情介绍)','')
                //key = key.replace(' ', '')
                //key = key.replace('　', '')
                //key = key.replace('：', '')
                let key = $(this).prev('.entry_b_tit').find('h2').html()
				key = key.replace(name, '')
                if (key != '1234') {
                    entry_detail.push({ key:key.trim(), value:value.trim() })
                }
            })

            $('.replace_title').remove()
            let petInfo = {
                name: name,
                entry_tit_pet: entry_tit_pet,
                entry_detail: entry_detail,
                imgs: [...imgs]
            }
            //console.log(petInfo)
            return petInfo
        })
        //let pinyinming=pinyinlite(petInfo.name).join('-')
        //petInfo.pinyinming=pinyinming
        petInfo['id'] = id
        let folderName = id
        //petInfo.ename.replace(' ', '-')
        await fs.mkdirSync(`./${source.name}/${folderName}/`)
        let imgsCount = 0
        for (let i in petInfo.imgs) {
            try {
                const options = {
                    url: petInfo.imgs[i],
                    dest: `./${source.name}/${folderName}/${parseInt(i) + 1}.jpg`                  // Save to /path/to/dest/image.jpg
                }

                await download.image(options)
                imgsCount++
            } catch (err) {

            }
            //console.log('File saved to', filename, image)

        }
        delete petInfo.imgs
        petInfo['imgs_count'] = imgsCount
        await page.waitFor(88);
        return petInfo
    }


    async function findDetailLink(url) {
        await page.goto(url)
        await navigationPromise
        //const pages = await page.$$('#page a');
        //console.log(pages)


        let pagesList = await page.evaluate((url) => {
            let pages = []
            let lastPage = $("#page a:contains('下一页')").prev('a').html()
            lastPage = parseInt(lastPage)
            //console.log('lastPage', lastPage)
            for (let i = 1; i <= lastPage; i++) {
                pages.push(`${url}?p=${i}`)
            }
            /*    

            $("#page:first a[href^='http']").each(function () {
                //console.log(this)
                pages.push($(this).attr('href'))
            })*/
            return pages
        }, url)
        let pages = new Set(pagesList)
        //console.log(pages)
        let allPets = []
        for (let p of pages.values()) {
            await page.goto(p, {
                waitUntil: 'domcontentloaded'
            })
            await navigationPromise

            let pets = await page.evaluate(() => {
                let pets = []
                $(".hot_pet_cont:first dd a[href^='http']").each(function () {
                    //console.log(this)
                    pets.push($(this).attr('href'))
                })
                return pets
            })
            console.log("pets", pets.length)
            allPets = allPets.concat(pets)

        }
        //console.log("allPets", allPets.length)
        let pets = new Set(allPets)
        //console.log(pets)
        return pets


    }

})();